<?php


//define addition function
function add2numbers($number1,$number2)
{
    $result=$number1+$number2;
    return $result;
}

//define subtract function
function sub2numbers($number1,$number2){
    $result=$number1-$number2;
    return $result;
}

//define multiplication function
function mult2numbers($number1,$number2){
    $result=$number1*$number2;
    return $result;
}

//define division function
function div2numbers($number1,$number2){
    $result=$number1/$number2;
    return $result;
}

//three types of presentation

function DisplaySimple($story){
    echo $story;
}

function DisplayPre($story){
    echo "<pre>";
    echo $story;
    echo "</pre>";
}

function DisplayH1($story){
    echo "<h1>";
    echo $story;
    echo "</h1>";
}